package com.sap.hana.cloud.hh.sapcoder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;


@Path("/getResume")
@Produces({ MediaType.APPLICATION_JSON })
public class MainController {
	@GET
	@Path("/")
	@Produces({ MediaType.TEXT_PLAIN })
	public String getResume(@Context SecurityContext ctx) {
		String retVal = "tempString";
		return retVal;
	}
}
