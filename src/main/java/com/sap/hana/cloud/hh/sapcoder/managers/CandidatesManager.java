package com.sap.hana.cloud.hh.sapcoder.managers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sap.hana.cloud.hh.sapcoder.ResultSetSerializer;

public class CandidatesManager {
	SimpleModule module = new SimpleModule();
	ObjectMapper objectMapper = new ObjectMapper();
	
	
	public CandidatesManager() {
		module.addSerializer(new ResultSetSerializer());
		objectMapper.registerModule(module);
	}
	
	public void writeCandidates(PrintWriter stringWriter) {
		Connection connection = null;
		try {
			Class.forName("com.sap.db.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection("jdbc:sap://localhost:30015/", "SYSTEM", "Arcadia12345678");
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			System.err.println("Connection Failed. User/Passwd Error?");
			return ;
		} 
		if (connection != null) {
			try {
				System.out.println("Connection to HANA successful!");
				Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("Select * from HH.RESUMES");
				ObjectNode objectNode = objectMapper.createObjectNode();
				objectNode.putPOJO("results", resultSet);
				objectMapper.writeValue(stringWriter, objectNode);

//				resultSet.next();
//				String hello = resultSet.getString(1);
//				return ;
			} catch (SQLException | IOException e) {
				System.err.println("Query failed!");
			}
		}
		return ;
	}

}
