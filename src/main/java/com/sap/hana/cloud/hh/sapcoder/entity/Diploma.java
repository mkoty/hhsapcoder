package com.sap.hana.cloud.hh.sapcoder.entity;

public class Diploma {
	private Integer organization_id;
	private Integer name_id;
	private String name;
	private int year;
	private String organization;
	private int result_id;
	private String result;
	
	
	public Diploma(Integer organization_id, Integer name_id, String name, int year, String organization, int result_id,
			String result) {
		super();
		this.organization_id = organization_id;
		this.name_id = name_id;
		this.name = name;
		this.year = year;
		this.organization = organization;
		this.result_id = result_id;
		this.result = result;
	}
	
	public Integer getOrganization_id() {
		return organization_id;
	}
	public void setOrganization_id(Integer organization_id) {
		this.organization_id = organization_id;
	}
	public Integer getName_id() {
		return name_id;
	}
	public void setName_id(Integer name_id) {
		this.name_id = name_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public int getResult_id() {
		return result_id;
	}
	public void setResult_id(int result_id) {
		this.result_id = result_id;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
}
