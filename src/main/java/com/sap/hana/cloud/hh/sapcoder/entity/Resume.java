package com.sap.hana.cloud.hh.sapcoder.entity;

import java.util.Date;
import java.util.List;

public class Resume {
	
	private String id;
	private String title;
	private String last_name;
	private String first_name;
	private String middle_name;
//	private String skills;
	private String alternate_url;
	
	private int score;
	private int age;
	
//	private BaseEntity resume_locale;
	
	private Date created_at;
	private Date updated_at;
	private Date birth_date;
	
//	private BaseEntity business_trip_readiness;
//	private BaseEntity travel_time;
	private Salary salary;
	
//	private List<BaseEntity> employments;
	
	
	private List<Specialization> specialization;
	private List<Experience> experience;
//	private List<Object> contact;
//	private List<Object> language;
	private List<String> skill_set;
	
	private Education education;
	private Area area;
	private TotalExperience total_experience;
	private BaseEntity gender;
//	private List<Object> schedules;
	
	
	public Resume(String id, String title, String last_name, String first_name, String middle_name,
			String alternate_url, int score, int age, Date created_at, Date updated_at, Date birth_date, Salary salary,
			List<Specialization> specialization, List<Experience> experience, List<String> skill_set,
			Education education, Area area, TotalExperience total_experience, BaseEntity gender) {
		
		this.id = id;
		this.title = title;
		this.last_name = last_name;
		this.first_name = first_name;
		this.middle_name = middle_name;
		this.alternate_url = alternate_url;
		this.score = score;
		this.age = age;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.birth_date = birth_date;
		this.salary = salary;
		this.specialization = specialization;
		this.experience = experience;
		this.skill_set = skill_set;
		this.education = education;
		this.area = area;
		this.total_experience = total_experience;
		this.gender = gender;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getAlternate_url() {
		return alternate_url;
	}
	public void setAlternate_url(String alternate_url) {
		this.alternate_url = alternate_url;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	public Salary getSalary() {
		return salary;
	}
	public void setSalary(Salary salary) {
		this.salary = salary;
	}
	public List<Specialization> getSpecialization() {
		return specialization;
	}
	public void setSpecialization(List<Specialization> specialization) {
		this.specialization = specialization;
	}
	public List<Experience> getExperience() {
		return experience;
	}
	public void setExperience(List<Experience> experience) {
		this.experience = experience;
	}
	public List<String> getSkill_set() {
		return skill_set;
	}
	public void setSkill_set(List<String> skill_set) {
		this.skill_set = skill_set;
	}
	public Education getEducation() {
		return education;
	}
	public void setEducation(Education education) {
		this.education = education;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public TotalExperience getTotal_experience() {
		return total_experience;
	}
	public void setTotal_experience(TotalExperience total_experience) {
		this.total_experience = total_experience;
	}
	public BaseEntity getGender() {
		return gender;
	}
	public void setGender(BaseEntity gender) {
		this.gender = gender;
	}
}
