package com.sap.hana.cloud.hh.sapcoder.entity;

public class Area extends BaseEntity {
	
	public Area(String id, String name, String url) {
		super(id, name);
		this.url = url;
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
