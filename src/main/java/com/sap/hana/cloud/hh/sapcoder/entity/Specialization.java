package com.sap.hana.cloud.hh.sapcoder.entity;

public class Specialization {

	private int profarea_id;
	private String profarea_name;
	private float id;
	private boolean laboring;
	private String name;
	
	public Specialization(int profarea_id, String profarea_name, float id, boolean laboring, String name) {
		super();
		this.profarea_id = profarea_id;
		this.profarea_name = profarea_name;
		this.id = id;
		this.laboring = laboring;
		this.name = name;
	}
	
	public int getProfarea_id() {
		return profarea_id;
	}
	public void setProfarea_id(int profarea_id) {
		this.profarea_id = profarea_id;
	}
	public String getProfarea_name() {
		return profarea_name;
	}
	public void setProfarea_name(String profarea_name) {
		this.profarea_name = profarea_name;
	}
	public float getId() {
		return id;
	}
	public void setId(float id) {
		this.id = id;
	}
	public boolean isLaboring() {
		return laboring;
	}
	public void setLaboring(boolean laboring) {
		this.laboring = laboring;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
