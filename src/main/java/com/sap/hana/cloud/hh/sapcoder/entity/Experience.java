package com.sap.hana.cloud.hh.sapcoder.entity;

import java.util.Date;
import java.util.List;

public class Experience {
	
	private List<BaseEntity> industries;
	private Date start;
	private Date end;
	private String description;
	
	public Experience(List<BaseEntity> industries, Date start, Date end, String description) {
		this.industries = industries;
		this.start = start;
		this.end = end;
		this.description = description;
	}
	
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<BaseEntity> getIndustries() {
		return industries;
	}
	public void setIndustries(List<BaseEntity> industries) {
		this.industries = industries;
	}

}
