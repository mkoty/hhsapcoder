package com.sap.hana.cloud.hh.sapcoder.entity;

public class Salary {
	
	private String currency;
	private int amount;
	
	public Salary(String currency, int amount) {
		this.currency = currency;
		this.amount = amount;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}

}
