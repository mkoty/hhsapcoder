package com.sap.hana.cloud.hh.sapcoder.entity;

public class TotalExperience {
	
	private int months;
	
	public TotalExperience(int months) {
		super();
		this.months = months;
	}

	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

}
