package com.sap.hana.cloud.hh.sapcoder.entity;

import java.util.List;

public class Education {
	
	private List<Diploma> primary;
	private List<Diploma> elementary;
	private List<Diploma> additional;
	private List<Diploma> attestation;
	
	private BaseEntity level;

	public List<Diploma> getPrimary() {
		return primary;
	}

	public void setPrimary(List<Diploma> primary) {
		this.primary = primary;
	}

	public List<Diploma> getElementary() {
		return elementary;
	}

	public void setElementary(List<Diploma> elementary) {
		this.elementary = elementary;
	}

	public List<Diploma> getAdditional() {
		return additional;
	}

	public void setAdditional(List<Diploma> additional) {
		this.additional = additional;
	}

	public List<Diploma> getAttestation() {
		return attestation;
	}

	public void setAttestation(List<Diploma> attestation) {
		this.attestation = attestation;
	}

	public BaseEntity getLevel() {
		return level;
	}

	public void setLevel(BaseEntity level) {
		this.level = level;
	}
}
